## Synopsis

Application using React, Redux, Express, and Node. 

## Installation
1. Have MongoDB running
2. Clone application - `git clone https://gitlab.com/squireaj/Optimum-challange.git` 
3. install dependencies - $npm install
5. Run - $npm run webpack
6. Open a new tab
7. Run - $npm start
8. Navigate to http://localhost:9000

## App 
The app may be used to add users and trait scores. Or you can use the methods provided below. 

## API Reference

You can find a Postman Collection at this link:
https://www.getpostman.com/collections/b4aa0a50302a91950477

##### To use Postman Collection:
1. Go to: Collection/Import/Import From Link 
2. Paste the link in the provided field 
3. Click import

#### To use CURL or other client: 
To add a user:
* POST
* URL: localhost:9000/api/user/NewUser
* Sample: `{
           	"Name": "Coop",
           	"IsActive": true
           }`

To add a TraitScore: 
* POST
* URL: localhost:9000/api/traitScore/newTrait
* Sample: `{
           	"AppUser": "5a5c09e2c2f9bf211bf6ad6e",
           	"Score": 6,
           	"Trait": "FooBar",
           	"IsValid": true
           }`
           
To get filtered users: 
* GET 
* URL: localhost:9000/api/user/withTrait/FooBar

## License

MIT