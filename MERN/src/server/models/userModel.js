
const mongoose = require('mongoose'),
      Schema   = mongoose.Schema;


const UserSchema = new Schema({
      Name        :  {type: String},
      CreatedDate :  {type: Date, default: Date.now()},
      IsActive    :  {type: Boolean, default: true},
      TraitScores : [{type: Schema.Types.ObjectId, ref: 'TraitScore', default: []}]
});

module.exports = mongoose.model('User', UserSchema);