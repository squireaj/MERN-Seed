import {ADD_TRAIT_TO_USER, FETCH_TRAITS, FETCH_USER} from "../actions";
import _ from 'lodash';


export default function (state = {}, action) {
    switch (action.type){
        case FETCH_TRAITS:
            return Object.assign({}, state,{
                traits: _.mapKeys(action.payload.data, "_id")
            });
        default:
            return state
    }
}
