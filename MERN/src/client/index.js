
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import promise from 'redux-promise';
import './scss/application.scss';
import reducers from './reducers/index';
import Nav from './components/Nav.js';
import Page1 from './components/Page1.js';
import Page2 from './components/Page2.js';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);


ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <div className="App">
            <BrowserRouter>
                <div>
                    <Nav />
                    <Switch>
                        <Route path="/" exact component={Page1} />
                        <Route path="/page2" exact component={Page2} />
                    </Switch>
                </div>
            </BrowserRouter>
            </div>
    </Provider>
    , document.getElementById("root"));
