import React, {Component} from 'react';
import {Link} from 'react-router-dom';


class Nav extends Component{

    constructor(props) {
        super(props);
        this.state = {
            activeTab: "assignment1"
        };
    }
    changeActiveTab(activeTab) {
        return this.setState({activeTab});
    };

    render(){
        const {activeTab} = this.state;
        return(
        <div>
            <header className="App-header">
                <div className="App-header-left">
                    <div className="App-title">MERN</div>
                    <div className="App-title">SEED</div>
                </div>
                <div className="App-header-right">
                    <Link to="/" className={activeTab == "assignment1" ? "App-tab App-tab-active" : "App-tab"} onClick={()  => this.changeActiveTab("assignment1")}>PAGE 1</Link>
                    <Link to="/page2" className={activeTab == "assignment2" ? "App-tab App-tab-active" : "App-tab"} onClick={()  => this.changeActiveTab("assignment2")}>PAGE 2</Link>
                </div>
            </header>
            <div className="horizontal-line"/>
        </div>
        )
    }

}

export default Nav
